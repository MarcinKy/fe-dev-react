import React from 'react';
import './About.css';

function About() {
  return (
    <section className="about" id="about">
    <div className="pseudo-box">
        <h1>Nasi specjaliści</h1>
        <div className="about-specialist">
            <div className="about-specialist-photo about-specialist-photo--employee1"></div>
            <div className="about-specialist-description">
                <h2>Imie nazwisko [dział]</h2>
                <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Architecto impedit 
                    vel animi sapiente vitae modi repudiandae, recusandae beatae aut blanditiis corrupti 
                    id officiis? Est quod laboriosam consequuntur suscipit assumenda corrupti.
                </p>
            </div>
        </div>
        <div className="about-specialist">           
            <div className="about-specialist-photo about-specialist-photo--employee2"></div>
            <div className="about-specialist-description">
                <h2>Imie nazwisko [dział]</h2>
                <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Architecto impedit 
                    vel animi sapiente vitae modi repudiandae, recusandae beatae aut blanditiis corrupti 
                    id officiis? Est quod laboriosam consequuntur suscipit assumenda corrupti.
                </p>
            </div>
        </div>
        <div className="about-specialist">           
            <div className="about-specialist-photo about-specialist-photo--employee3"></div>
            <div className="about-specialist-description">
                <h2>Imie nazwisko [dział]</h2>
                <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Architecto impedit 
                    vel animi sapiente vitae modi repudiandae, recusandae beatae aut blanditiis corrupti 
                    id officiis? Est quod laboriosam consequuntur suscipit assumenda corrupti.
                </p>
            </div>
        </div>
    </div>
</section>
  );
}

export default About;
