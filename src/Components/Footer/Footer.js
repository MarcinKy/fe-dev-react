import React from 'react';
import './Footer.css';
import { FaFacebookSquare } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";

function Footer() {
  return (
    <footer>
        <div className="pseudo-box">
            <p>nazwa firmy - wszelkie prawa zastrzeżone, 2020</p>            
            <div>
                <a className="footer-react-icons" href="https://facebook.com/"><FaFacebookSquare /></a> 
                <a className="footer-react-icons" href="https://instagram.com/"><FaInstagram /></a>
            </div>
        </div>
    </footer>
  );
}

export default Footer;
