import React from 'react';
import './LandingPage.css';

function LandingPage() {
  return (
    <section className="landing" id="landing">
      <div className="landing-shadow">
        <div className="pseudo-box"> 
          <div className="landing-text-area"> 
            <h1>Nasza firma oferuje najwyższej jakości produkty</h1>
            <h2>Nie wierz nam na słowo - sprawdź</h2>
            <a className="landing-cta" href="#services">Oferta</a>                                                
          </div>
        </div>
      </div>
    </section>
  );
}

export default LandingPage;
