import React from 'react';
import './Nav.css';

function Nav() {
  return (
    <nav>            
      <div className="pseudo-box"> 
        <a href="#landing" className="company-name">nazwa firmy</a>
        <input className="nav-menu-btn" type="checkbox" id="nav-menu-btn"/>
        <label className="nav-menu-icon" for="nav-menu-btn"> <span className="nav-icon-burger"></span></label>                           
        <ul className="nav-menu">
          <li><a href="#about">o nas</a></li>
          <li><a href="#services">oferta</a></li>
          <li><a href="#contact" className="nav-items-disabled">kontakt</a></li>
        </ul>
      </div>
    </nav>
  );
}

export default Nav;
