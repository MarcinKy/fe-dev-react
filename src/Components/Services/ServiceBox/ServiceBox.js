import React from 'react';
import './ServiceBox.css';

function ServiceBox(props) {

  return (
    <div className="service-box"> 
        <div className="service-box-content"> 
            <div className="service-box-featuring-icon">{props.featuringIcon}</div>         
            <h2>{props.title}</h2><p>{props.desc}</p>
        </div>
    </div>               
  );
}

export default ServiceBox;
