import React from 'react';
import './ServiceBoxNew.css';

function ServiceBoxNew() {
  return (
    <div className="service-box"> 
        <div className="service-box-content">
            <div className="service-box-dot"></div>
            <h2>Usługa 1</h2><p>(nowość)</p>
        </div>
    </div>               
  );
}

export default ServiceBoxNew;
