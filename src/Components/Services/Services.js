import React from 'react';
import './Services.css';
import ServiceBox from './ServiceBox/ServiceBox';
// import ServiceBoxNew from './ServiceBoxNew/ServiceBoxNew';
import { MdStar } from 'react-icons/md';
import { MdStarBorder } from 'react-icons/md';
import { MdStarHalf } from 'react-icons/md';
import { MdBrightness4 } from 'react-icons/md';
import { MdBrightness5 } from 'react-icons/md';
import { MdBrightness6 } from 'react-icons/md';

function Services() {
  let serviceList = [
    {title: "Najważniejsza usługa", desc: "Ta usługa jest podstawą naszej egzystencji, bez niej - nas nie ma...", featuringIcon:  <MdStar /> },
    {title: "Ważna usługa", desc: "Ta usługa jest ważna, a jednocześnie pożyteczna. Czy już jej próbowałeś?", featuringIcon: <MdStarBorder /> },
    {title: "Przefajna usługa", desc: "No, to jest dynamit. Musisz spróbować, jak nie zadziała zwracamy pieniądze!", featuringIcon: <MdStarHalf /> }, 
    {title: "Średnio znana usługa", desc: "Mało o niej słychać... pewnie dlatego że jest prestiżowa.", featuringIcon: <MdBrightness4 /> },
    {title: "Gamechanger", desc: "Petarda. I nie ma w tym cny przesady. Czy zawsze czułeś że czegoś Ci brakuje?", featuringIcon: <MdBrightness5 /> },
    {title: "Zwyczajna usługa", desc: "Całkowicie zwyczajna usługa, taka jakaś niepozorna, ale... do życia niezbędna.", featuringIcon: <MdBrightness6 /> },  
  ]
  return (
    <section className="services" id="services">
        <div className="pseudo-box">
            <h1>Czym zajmuje się nasza firma?</h1>
            <div className="service-wrapper"> 
              {
               serviceList.map((serviceOnList) => <ServiceBox title={serviceOnList.title} desc={serviceOnList.desc} featuringIcon={serviceOnList.featuringIcon} ></ServiceBox>)
              }
            </div>
        </div>
    </section>
  );
}

export default Services;
